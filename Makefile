CC = gcc
CFLAGS = -Wall -Werror
default: all
all: Hello
Hello: main.c
	$(CC) $(CFLAGS) -o hello main.c
.PHONY: clean
clean:
	rm hello
